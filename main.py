import os
import pika
import json
import logging
import time
import threading

import Broker
import Providers

from ptl_api import new_client, Configuration
from swagger_codegen.api.adapter.requests import RequestsAdapter


QUEUE_MAPPED_MAIN = "/ptl/mapper/main"


if __name__ == "__main__":
    host = (
        "http://" + os.getenv("API_HOST", "api") + ":" + os.getenv("API_PORT", "8000")
    )
    client = new_client(RequestsAdapter(), Configuration(host=host))
    RABBIT_MQ_URL = os.getenv("RABBIT_MQ_URL", "broker")
    logging.basicConfig(
        format="%(asctime)s %(levelname)s %(message)s",
        level=logging.INFO,
        datefmt="[%d/%m/%Y %H:%M:%S]",
    )

    channel, connection = Broker.Setup(RABBIT_MQ_URL, [QUEUE_MAPPED_MAIN])

    while True:
        try:
            setting = client.settings.settings_retrieve("MODE")
            if setting.value == "PROD":
                if channel.is_closed or connection.is_closed:
                    channel, connection = Broker.Setup(RABBIT_MQ_URL, QUEUE_MAPPED_MAIN)
                x = threading.Thread(
                    target=Providers.aquiweb, args=(channel, QUEUE_MAPPED_MAIN, client)
                )
                x.start()
                time.sleep(1)
            else:
                logging.info(
                    "[MAIN] Application in <"
                    + str(setting.value)
                    + "> mode. No need to scrap. Sleeping for 15sec..."
                )
                time.sleep(15)
        except KeyboardInterrupt as ex:
            logging.info("[MAIN] STOP process due to keyboard interrupt")
            break
        except Exception as ex:
            logging.exception(ex, exc_info=True)
    connection.close()
    logging.info("[MAIN] Bye")

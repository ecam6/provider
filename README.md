# Provider Pick to light

Ce projet appartient au projet [pick to light](https://gitlab.com/ecam6/ptl_apps) et se base sur [Python](https://www.python.org/).

Son rôle est d'écouter le MES et d'envoyer un message dès qu'il y a un changement d'état.

# Documentation

Un changement d'état correspond à un changement d'étape dans un mode opératoire du MES.

Pour detecter cela dans Aquiweb, nous devons scrapper la base de données (ici toutes les secondes).

Grâce à une seule requête et une petite gymnastique d'esprit nous pouvons détecter l'allumage et l'extinction des LEDs.

Les tables écoutées sont les suivantes (la requête peut-être trouvé dans le code):
- public.event_personne
- public.guidage_operation_guidage
- public.guidage_phase_guidage

`Broker.py` contient les éléments de connexion et reconnexion au message broker.

`main.py` est le point d'entrée du programme, connexion, la boucle de scrap...

`Providers.py` les différents moyens de récupérer les informations suivants les providers (pour le moment juste aquiweb)

# Variable d'environement

- RABBIT_MQ_URL: (default: broker)
- API_HOST: (default api)
- API_PORT: (default 8000)
- AQUIWEB_DB_HOST (default: divalto)
- AQUIWEB_DB_NAME (default: aquiweb)
- AQUIWEB_DB_USER (default: user)
- AQUIWEB_DB_PWD (default: $)


# Déploiement, usage, configuration...

Ne peut pas être utilisé seul, se référé au [projet principal](https://gitlab.com/ecam6/ptl_apps)

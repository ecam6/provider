from __future__ import annotations

import datetime
import pydantic
import typing

from pydantic import BaseModel

from swagger_codegen.api.base import BaseApi
from swagger_codegen.api.request import ApiRequest
from swagger_codegen.api import json
class Box(BaseModel):
    article: typing.Optional[typing.Union[str, None]]  = None
    article_name: str 
    col_size: int 
    id: str 
    is_on: typing.Optional[bool]  = None
    line: str 
    pos: int 
    size: str 

class Line(BaseModel):
    boxes: typing.Optional[typing.List[Box]]  = None
    id: str 
    is_full: bool 
    pos: int 
    workstation: str 

class Workstation(BaseModel):
    id: str 
    id_ext: str 
    iolink: typing.Optional[typing.Union[str, None]]  = None
    iolink_port: typing.Optional[typing.Union[int, None]]  = None
    lines: typing.Optional[typing.List[Line]]  = None
    pos: int 

class PaginatedWorkstationList(BaseModel):
    count: typing.Optional[int]  = None
    next: typing.Optional[typing.Union[str, None]]  = None
    previous: typing.Optional[typing.Union[str, None]]  = None
    results: typing.Optional[typing.List[Workstation]]  = None

def make_request(self: BaseApi,


    page: int = ...,

) -> PaginatedWorkstationList:
    

    
    body = None
    

    m = ApiRequest(
        method="GET",
        path="/workstations/".format(
            
        ),
        content_type=None,
        body=body,
        headers=self._only_provided({
        }),
        query_params=self._only_provided({
                "page": page,
            
        }),
        cookies=self._only_provided({
        }),
    )
    return self.make_request({
    
        "200": {
            
                "application/json": PaginatedWorkstationList,
            
        },
    
    }, m)
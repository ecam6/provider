from __future__ import annotations

from swagger_codegen.api.base import BaseApi

from . import api_schema_retrieve
class ApiApi(BaseApi):
    api_schema_retrieve = api_schema_retrieve.make_request
from __future__ import annotations

from swagger_codegen.api.base import BaseApi

from . import settings_list
from . import settings_create
from . import settings_retrieve
from . import settings_update
from . import settings_partial_update
from . import settings_destroy
class SettingsApi(BaseApi):
    settings_list = settings_list.make_request
    settings_create = settings_create.make_request
    settings_retrieve = settings_retrieve.make_request
    settings_update = settings_update.make_request
    settings_partial_update = settings_partial_update.make_request
    settings_destroy = settings_destroy.make_request
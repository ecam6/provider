from __future__ import annotations

import datetime
import pydantic
import typing

from pydantic import BaseModel

from swagger_codegen.api.base import BaseApi
from swagger_codegen.api.request import ApiRequest
from swagger_codegen.api import json
class PatchedBox(BaseModel):
    article: typing.Optional[typing.Union[str, None]]  = None
    is_on: typing.Optional[bool]  = None
    line: typing.Optional[str]  = None
    pos: typing.Optional[int]  = None
    size: typing.Optional[str]  = None

class Box(BaseModel):
    article: typing.Optional[typing.Union[str, None]]  = None
    article_name: str 
    col_size: int 
    id: str 
    is_on: typing.Optional[bool]  = None
    line: str 
    pos: int 
    size: str 

def make_request(self: BaseApi,

    __request__: PatchedBox,


    id: str,

) -> Box:
    

    
    body = __request__
    

    m = ApiRequest(
        method="PATCH",
        path="/boxes/{id}/".format(
            
                id=id,
            
        ),
        content_type="application/json",
        body=body,
        headers=self._only_provided({
        }),
        query_params=self._only_provided({
        }),
        cookies=self._only_provided({
        }),
    )
    return self.make_request({
    
        "200": {
            
                "application/json": Box,
            
        },
    
    }, m)
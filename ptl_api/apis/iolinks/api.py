from __future__ import annotations

from swagger_codegen.api.base import BaseApi

from . import iolinks_list
from . import iolinks_create
from . import iolinks_retrieve
from . import iolinks_update
from . import iolinks_partial_update
from . import iolinks_destroy
class IolinksApi(BaseApi):
    iolinks_list = iolinks_list.make_request
    iolinks_create = iolinks_create.make_request
    iolinks_retrieve = iolinks_retrieve.make_request
    iolinks_update = iolinks_update.make_request
    iolinks_partial_update = iolinks_partial_update.make_request
    iolinks_destroy = iolinks_destroy.make_request
from __future__ import annotations

import datetime
import pydantic
import typing

from pydantic import BaseModel

from swagger_codegen.api.base import BaseApi
from swagger_codegen.api.request import ApiRequest
from swagger_codegen.api import json
class Box(BaseModel):
    article: typing.Optional[typing.Union[str, None]]  = None
    article_name: str 
    col_size: int 
    id: str 
    is_on: typing.Optional[bool]  = None
    line: str 
    pos: int 
    size: str 

class Article(BaseModel):
    boxes: typing.Optional[typing.List[Box]]  = None
    id: str 
    id_ext: str 
    name: str 

def make_request(self: BaseApi,


) -> typing.List[Article]:
    

    
    body = None
    

    m = ApiRequest(
        method="GET",
        path="/articles/".format(
            
        ),
        content_type=None,
        body=body,
        headers=self._only_provided({
        }),
        query_params=self._only_provided({
        }),
        cookies=self._only_provided({
        }),
    )
    return self.make_request({
    
        "200": {
            
                "application/json": typing.List[Article],
            
        },
    
    }, m)
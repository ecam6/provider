from __future__ import annotations

import datetime
import pydantic
import typing

from pydantic import BaseModel

from swagger_codegen.api.base import BaseApi
from swagger_codegen.api.request import ApiRequest
from swagger_codegen.api import json
class Event(BaseModel):
    id: str 
    id_article_ext: str 
    id_ext: str 
    id_workstation_ext: str 

def make_request(self: BaseApi,


    id: str,

) -> Event:
    

    
    body = None
    

    m = ApiRequest(
        method="GET",
        path="/events/{id}/".format(
            
                id=id,
            
        ),
        content_type=None,
        body=body,
        headers=self._only_provided({
        }),
        query_params=self._only_provided({
        }),
        cookies=self._only_provided({
        }),
    )
    return self.make_request({
    
        "200": {
            
                "application/json": Event,
            
        },
    
    }, m)
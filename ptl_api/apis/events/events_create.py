from __future__ import annotations

import datetime
import pydantic
import typing

from pydantic import BaseModel

from swagger_codegen.api.base import BaseApi
from swagger_codegen.api.request import ApiRequest
from swagger_codegen.api import json
class Event(BaseModel):
    id_article_ext: str 
    id_ext: str 
    id_workstation_ext: str 

def make_request(self: BaseApi,

    __request__: Event,


) -> Event:
    

    
    body = __request__
    

    m = ApiRequest(
        method="POST",
        path="/events/".format(
            
        ),
        content_type="application/json",
        body=body,
        headers=self._only_provided({
        }),
        query_params=self._only_provided({
        }),
        cookies=self._only_provided({
        }),
    )
    return self.make_request({
    
        "201": {
            
                "application/json": Event,
            
        },
    
    }, m)
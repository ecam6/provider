import Broker
import logging
import time
import random
import os
import json
import psycopg2


def get_data(state, id_workstation, id_article):
    return json.dumps({"state": state, "id_workstation_ext": id_workstation, "article": id_article})

def aquiweb(channel, routing_key, client):
    conn = None
    try:
        conn = psycopg2.connect(
            host=os.getenv("AQUIWEB_DB_HOST", "divalto"),
            database=os.getenv("AQUIWEB_DB_NAME", "aquiweb"),
            user=os.getenv("AQUIWEB_DB_USER", "user"),
            password=os.getenv("AQUIWEB_DB_PWD", "password"),
        )
        cur = conn.cursor()
        cur.execute(
            "SELECT id_event, id_statut, event_personne.id_equipement as id_equipement, id_operation_guidage, id_phase_guidage, display_indicators \
                    FROM public.event_personne \
                    LEFT JOIN public.guidage_operation_guidage ON public.event_personne.id_operation_guidage = public.guidage_operation_guidage.id \
                    LEFT JOIN public.guidage_phase_guidage ON public.guidage_operation_guidage.id_phase_guidage = public.guidage_phase_guidage.id \
                    WHERE date_end is null"
        )
        rows = cur.fetchall()
        col_id_event = 0
        col_id_equipement = 2
        col_display_indicator = 5

        events = client.events.events_list().results
        for row in rows:  # if I have some results and not already in my db so I send ON
            if not [
                e for e in events if e.id_ext == str(row[col_id_event])
            ]:  # if empty results
                id_workstation = row[col_id_equipement]
                if (
                    row[col_display_indicator] is None
                ):  # Case if there is a line in DB but nothing is assign to it (LEFT JOIN)
                    continue
                elements = json.loads(row[col_display_indicator])[0]["value"].split(
                    "_"
                )  # display_indicators (ex) : 1 x 75-1911210 _ 3 x 75-1911745 _ 3 x 75-1911935
                for element in elements:
                    element_split = element.split("x ")
                    if len(element_split) != 2:
                        logging.warning(
                            "[x] Can't send ON, receive unkown format of article <"
                            + str(element)
                            + ">"
                        )
                        client.events.events_create(
                            {
                                "id_ext": str(row[col_id_event]),
                                "id_workstation_ext": str(id_workstation),
                                "id_article_ext": str(None),
                            }
                        )
                        continue
                    id_article = element_split[1].strip()
                    Broker.send(channel, routing_key, get_data(True, id_workstation, id_article))
                    client.events.events_create({'id_ext': str(row[col_id_event]), 'id_workstation_ext': str(id_workstation), 'id_article_ext': str(id_article)})
                    time.sleep(3) # Sleep between article
                    
        for event in events: # if I don't have record in row but in my DB send OFF
            if not [r for r in rows if str(r[col_id_event]) == event.id_ext]:
                Broker.send(
                    channel,
                    routing_key,
                    get_data(False, event.id_workstation_ext, event.id_article_ext),
                )
                try:
                    client.events.events_destroy(event.id)
                except KeyError:
                    pass
        cur.close()
        conn.close()
    except Exception as e:
        if conn is not None:
            conn.close()
        raise e
